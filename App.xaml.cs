﻿using Individuele_Opdracht___Facturatiesysteem.ViewModels;
using Individuele_Opdracht___Facturatiesysteem.Views;
using System.Windows;

namespace Individuele_Opdracht___Facturatiesysteem
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static MyUser LoggedInUser { get; set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            Window window = new MainWindow();
            window.DataContext = new MainViewModel();
            window.Show();

            base.OnStartup(e);
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            //KlantView klantView = new KlantView();
            //klantView.Show();

            //MainViewModel mainViewModel = new MainViewModel();
            //MainWindow mainWindow = new MainWindow();
            //mainWindow.Show();

            //StartView startView = new StartView();
            //startView.DataContext = new StartViewModel();
            //startView.Show();


            //LoginViewModel lvm = new LoginViewModel();
            //LoginView lv = new LoginView(lvm);

            //if(lv.ShowDialog() == true)
            //{
            //    StartViewModel viewmodel = new StartViewModel();
            //    StartView view = new StartView();

            //    view.DataContext = viewmodel;
            //    view.Show();
            //}

        }
    }
}

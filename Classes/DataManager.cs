﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individuele_Opdracht___Facturatiesysteem
{
    public class DataManager
    {
        #region Bestelling

        public static List<Bestelling> GetBestellingen()
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                return Mijn_DatabaseEntities.Bestelling.ToList();
            }
        }
        public static Bestelling GetBestellingByID(int bestellingid)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                var query = from bestelling in Mijn_DatabaseEntities.Bestelling
                            where bestelling.BestellingID == bestellingid
                            select bestelling;
                var w = query.FirstOrDefault();
                return w;
            }
        }
        public static int InsertBestelling(Bestelling b)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Bestelling.Add(b);
                if (0 < Mijn_DatabaseEntities.SaveChanges())
                {
                    return b.BestellingID;
                }
            }
            return 0;
        }
        public static int UpdateBestelling(Bestelling bestellingUpdate)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(bestellingUpdate).State = System.Data.Entity.EntityState.Modified;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }
        public static int DeleteBestelling(Bestelling bestellingDelete)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(bestellingDelete).State = System.Data.Entity.EntityState.Deleted;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }

        #endregion

        #region Categorie

        public static List<Categorie> GetCategorieen()
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                return Mijn_DatabaseEntities.Categorie.ToList();
            }
        }
        public static Categorie GetCategorieByID(int categorieid)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                var query = from categorie in Mijn_DatabaseEntities.Categorie
                            where categorie.CategorieID == categorieid
                            select categorie;
                var w = query.FirstOrDefault();
                return w;
            }
        }
        public static int InsertCategorie(Categorie c)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Categorie.Add(c);
                if (0 < Mijn_DatabaseEntities.SaveChanges())
                {
                    return c.CategorieID;
                }
            }
            return 0;
        }
        public static int UpdateCategorie(Categorie categorieUpdate)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(categorieUpdate).State = System.Data.Entity.EntityState.Modified;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }
        public static int DeleteCategorie(Categorie categorieDelete)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(categorieDelete).State = System.Data.Entity.EntityState.Deleted;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }

        #endregion

        #region Klant

        public static List<Klant> GetKlanten()
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                return Mijn_DatabaseEntities.Klant.ToList();
            }
        }
        public static Klant GetKlantByID(int klantid)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                var query = from klant in Mijn_DatabaseEntities.Klant
                            where klant.KlantID == klantid
                            select klant;
                var w = query.FirstOrDefault();
                return w;
            }
        }
        public static int InsertKlant(Klant k)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Klant.Add(k);
                if (0 < Mijn_DatabaseEntities.SaveChanges())
                {
                    return k.KlantID;
                }
            }
            return 0;
        }
        public static int UpdateKlant(Klant klantUpdate)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(klantUpdate).State = System.Data.Entity.EntityState.Modified;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }
        public static int DeleteKlant(Klant klantDelete)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(klantDelete).State = System.Data.Entity.EntityState.Deleted;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }

        #endregion

        #region Leverancier

        public static List<Leverancier> GetLeveranciers()
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                return Mijn_DatabaseEntities.Leverancier.ToList();
            }
        }
        public static Leverancier GetLeverancierByID(int leverancierid)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                var query = from leverancier in Mijn_DatabaseEntities.Leverancier
                            where leverancier.LeverancierID == leverancierid
                            select leverancier;
                var w = query.FirstOrDefault();
                return w;
            }
        }
        public static int InsertLeverancier(Leverancier l)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Leverancier.Add(l);
                if (0 < Mijn_DatabaseEntities.SaveChanges())
                {
                    return l.LeverancierID;
                }
            }
            return 0;
        }
        public static int UpdateLeverancier(Leverancier leverancierUpdate)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(leverancierUpdate).State = System.Data.Entity.EntityState.Modified;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }
        public static int DeleteLeverancier(Leverancier leverancierDelete)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(leverancierDelete).State = System.Data.Entity.EntityState.Deleted;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }

        #endregion

        #region Login

        public static List<Login> GetLogins()
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                return Mijn_DatabaseEntities.Login.ToList();
            }
        }

        public static Login GetLogin(string username, string password)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                var query = from Login in Mijn_DatabaseEntities.Login
                            where Login.Username == username
                            where Login.Paswoord == password
                            select Login;
                var w = query.FirstOrDefault();
                return w;
            }
        }

        public static Login GetLoginByUsername(string username)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                var query = from login in Mijn_DatabaseEntities.Login
                            where login.Username == username
                            select login;
                var w = query.FirstOrDefault();
                return w;
            }
        }

        public static int InsertLogin(Login l)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Login.Add(l);
                if (0 < Mijn_DatabaseEntities.SaveChanges())
                {
                    return 1;
                }
            }
            return 0;
        }

        public static int UpdateLogin(Login loginUpdate)
        {
            using(var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(loginUpdate).State = System.Data.Entity.EntityState.Modified;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }

        public static int DeleteLogin(Login loginDelete)
        {
            using(var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(loginDelete).State = System.Data.Entity.EntityState.Deleted;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }

        #endregion

        #region Personeelslid

        public static List<Personeelslid> GetPersoneelsleden()
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                return Mijn_DatabaseEntities.Personeelslid.ToList();
            }
        }
        public static Personeelslid GetPersoneelslidByID(int personeelslidid)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                var query = from personeelslid in Mijn_DatabaseEntities.Personeelslid
                            where personeelslid.PersoneelslidID == personeelslidid
                            select personeelslid;
                var w = query.FirstOrDefault();
                return w;
            }
        }
        public static int InsertPersoneelslid(Personeelslid p)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Personeelslid.Add(p);
                if (0 < Mijn_DatabaseEntities.SaveChanges())
                {
                    return p.PersoneelslidID;
                }
            }
            return 0;
        }
        public static int UpdatePersoneelslid(Personeelslid personeelslidUpdate)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(personeelslidUpdate).State = System.Data.Entity.EntityState.Modified;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }
        public static int DeletePersoneelslid(Personeelslid personeelslidDelete)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(personeelslidDelete).State = System.Data.Entity.EntityState.Deleted;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }

        #endregion

        #region Product

        public static List<Product> GetProducten()
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                return Mijn_DatabaseEntities.Product.ToList();
            }
        }
        public static Product GetProductByID(int productid)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                var query = from product in Mijn_DatabaseEntities.Product
                            where product.ProductID == productid
                            select product;
                var w = query.FirstOrDefault();
                return w;
            }
        }
        public static int InsertProduct(Product p)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Product.Add(p);
                if (0 < Mijn_DatabaseEntities.SaveChanges())
                {
                    return p.ProductID;
                }
            }
            return 0;
        }
        public static int UpdateProduct(Product productUpdate)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(productUpdate).State = System.Data.Entity.EntityState.Modified;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }
        public static int DeleteProduct(Product productDelete)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(productDelete).State = System.Data.Entity.EntityState.Deleted;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }

        #endregion

        #region Rol

        public static List<Rol> GetRollen()
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                return Mijn_DatabaseEntities.Rol.ToList();
            }
        }
        public static Rol GetRolByID(int rolid)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                var query = from rol in Mijn_DatabaseEntities.Rol
                            where rol.RolID == rolid
                            select rol;
                var w = query.FirstOrDefault();
                return w;
            }
        }
        public static int InsertRol(Rol r)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Rol.Add(r);
                if (0 < Mijn_DatabaseEntities.SaveChanges())
                {
                    return r.RolID;
                }
            }
            return 0;
        }
        public static int UpdateRol(Rol rolUpdate)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(rolUpdate).State = System.Data.Entity.EntityState.Modified;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }
        public static int DeleteRol(Rol rolDelete)
        {
            using (var Mijn_DatabaseEntities = new FacturatiesysteemDbEntities1())
            {
                Mijn_DatabaseEntities.Entry(rolDelete).State = System.Data.Entity.EntityState.Deleted;
                return Mijn_DatabaseEntities.SaveChanges();
            }
        }

        #endregion
    }
}

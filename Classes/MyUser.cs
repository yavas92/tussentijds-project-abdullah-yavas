﻿using Individuele_Opdracht___Facturatiesysteem;
using System.Security.Claims;
using System.Windows.Controls;

namespace Individuele_Opdracht___Facturatiesysteem
{
    public class MyUser
    {
        // Attributen
        private string _username;
        private string _password;
        public Roles Role { get; set; }

        // Constructor
        public MyUser() { }
        public MyUser(string username, string password, Roles role)
        {
            Username = username;
            Password = password;
            Role = role;
        }

        // Properties
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

    }
}
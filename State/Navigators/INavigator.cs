﻿using Individuele_Opdracht___Facturatiesysteem.ViewModels;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Individuele_Opdracht___Facturatiesysteem.State.Navigators
{
    public enum ViewType
    {
        Start,
        Login,
        Databeheer,
        Overzicht,
        Bestelling,
        LeverancierOverzicht,
        KlantOverzicht,
        ProductOverzicht,
        CategorieOverzicht,
        PersoneelslidOverzicht,
        LeverancierDatabeheer,
        KlantDatabeheer,
        ProductDatabeheer,
        CategorieDatabeheer,
        PersoneelslidDatabeheer
    }
    public interface INavigator
    {
        ViewModelBase CurrentViewModel { get; set; }
        ICommand UpdateCurrentViewModelCommand { get; }
    }
}

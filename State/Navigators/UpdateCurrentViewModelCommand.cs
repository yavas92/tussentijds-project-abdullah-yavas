﻿using Individuele_Opdracht___Facturatiesysteem.ViewModels;
using System;
using System.Windows.Input;

namespace Individuele_Opdracht___Facturatiesysteem.State.Navigators
{
    class UpdateCurrentViewModelCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private INavigator _navigator;

        public UpdateCurrentViewModelCommand(INavigator navigator)
        {
            _navigator = navigator;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (parameter is ViewType)
            {
                ViewType viewType = (ViewType)parameter;
                switch (viewType)
                {
                    case ViewType.Start:
                        _navigator.CurrentViewModel = new StartViewModel();
                        break;
                    case ViewType.Login:
                        _navigator.CurrentViewModel = new LoginViewModel();
                        break;
                    case ViewType.Databeheer:
                        _navigator.CurrentViewModel = new DatabeheerViewModel();
                        break;
                    case ViewType.Overzicht:
                        _navigator.CurrentViewModel = new OverzichtViewModel();
                        break;
                    case ViewType.Bestelling:
                        _navigator.CurrentViewModel = new BestellingViewModel();
                        break;
                    case ViewType.LeverancierOverzicht:
                        _navigator.CurrentViewModel = new LeverancierOverzichtViewModel();
                        break;
                    case ViewType.KlantOverzicht:
                        _navigator.CurrentViewModel = new KlantOverzichtViewModel();
                        break;
                    case ViewType.ProductOverzicht:
                        _navigator.CurrentViewModel = new ProductOverzichtViewModel();
                        break;
                    case ViewType.CategorieOverzicht:
                        _navigator.CurrentViewModel = new CategorieOverzichtViewModel();
                        break;
                    case ViewType.PersoneelslidOverzicht:
                        _navigator.CurrentViewModel = new PersoneelslidOverzichtViewModel();
                        break;
                    case ViewType.LeverancierDatabeheer:
                        _navigator.CurrentViewModel = new LeverancierDatabeheerViewModel();
                        break;
                    case ViewType.KlantDatabeheer:
                        _navigator.CurrentViewModel = new KlantDatabeheerViewModel();
                        break;
                    case ViewType.ProductDatabeheer:
                        _navigator.CurrentViewModel = new ProductDatabeheerViewModel();
                        break;
                    case ViewType.CategorieDatabeheer:
                        _navigator.CurrentViewModel = new CategorieDatabeheerViewModel();
                        break;
                    case ViewType.PersoneelslidDatabeheer:
                        _navigator.CurrentViewModel = new PersoneelslidDatabeheerViewModel();
                        break;
                    default:
                        break;
                }

            }
        }
    }
}
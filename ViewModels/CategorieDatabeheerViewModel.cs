﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class CategorieDatabeheerViewModel : ViewModelBase
    {
        private ObservableCollection<Categorie> _categorieen;

        public ObservableCollection<Categorie> Categorieen
        {
            get { return _categorieen; }
            set
            {
                _categorieen = value;
                OnPropertyChanged(nameof(Categorieen));
            }
        }


        public CategorieDatabeheerViewModel()
        {
            Categorieen = new ObservableCollection<Categorie>(DataManager.GetCategorieen());
        }
    }
}

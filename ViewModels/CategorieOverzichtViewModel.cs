﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class CategorieOverzichtViewModel:ViewModelBase
    {
        public ObservableCollection<Categorie> Categorieen { get; set; }

        public CategorieOverzichtViewModel()
        {
            Categorieen = new ObservableCollection<Categorie>(DataManager.GetCategorieen());
        }
    }
}

﻿using Individuele_Opdracht___Facturatiesysteem.Models;
using Individuele_Opdracht___Facturatiesysteem.View.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class KlantDatabeheerViewModel : ViewModelBase
    {
        private ObservableCollection<Klant> _klanten;
        private FacturatiesysteemDbEntities1 facturatiesysteemDb;

        public ObservableCollection<Klant> Klanten
        {
            get { return _klanten; }
            set
            {
                _klanten = value;
                OnPropertyChanged(nameof(Klanten));
            }
        }

        public RelayCommand SaveCommand { get; set; }

        public KlantDatabeheerViewModel()
        {
        facturatiesysteemDb = new FacturatiesysteemDbEntities1();

        Klanten = new ObservableCollection<Klant>(facturatiesysteemDb.Klant.ToList());
            SaveCommand = new RelayCommand((object obj) =>
            {
                facturatiesysteemDb.SaveChanges();
            });
        }

    }
}

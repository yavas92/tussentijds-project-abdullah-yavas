﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class KlantOverzichtViewModel : ViewModelBase
    {
        public ObservableCollection<Klant> Klanten { get; set; }

        public KlantOverzichtViewModel()
        {
            Klanten = new ObservableCollection<Klant>(DataManager.GetKlanten());
        }
    }
}

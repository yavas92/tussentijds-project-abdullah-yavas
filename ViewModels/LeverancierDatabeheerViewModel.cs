﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class LeverancierDatabeheerViewModel : ViewModelBase
    {
        private ObservableCollection<Leverancier> _leveranciers;

        public ObservableCollection<Leverancier> Leveranciers
        {
            get { return _leveranciers; }
            set
            {
                _leveranciers = value;
                OnPropertyChanged(nameof(Leveranciers));
            }
        }


        public LeverancierDatabeheerViewModel()
        {
            Leveranciers = new ObservableCollection<Leverancier>(DataManager.GetLeveranciers());
        }
    }
}

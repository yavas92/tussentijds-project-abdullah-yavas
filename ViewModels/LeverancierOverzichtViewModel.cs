﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class LeverancierOverzichtViewModel:ViewModelBase
    {
        public ObservableCollection<Leverancier> Leveranciers { get; set; }

        public LeverancierOverzichtViewModel()
        {
            Leveranciers = new ObservableCollection<Leverancier>(DataManager.GetLeveranciers());
        }
    }
}

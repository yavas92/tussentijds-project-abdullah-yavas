﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class LoginDatabeheerViewModel : ViewModelBase
    {
        private ObservableCollection<Login> _logins;

        public ObservableCollection<Login> Logins
        {
            get { return _logins; }
            set
            {
                _logins = value;
                OnPropertyChanged(nameof(Logins));
            }
        }


        public LoginDatabeheerViewModel()
        {
            Logins = new ObservableCollection<Login>(DataManager.GetLogins());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class LoginOverzichtViewModel : ViewModelBase
    {
        public ObservableCollection<Login> Logins { get; set; }

        public LoginOverzichtViewModel()
        {
            Logins = new ObservableCollection<Login>(DataManager.GetLogins());
        }
    }
}

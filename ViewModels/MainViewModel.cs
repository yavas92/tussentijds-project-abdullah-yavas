﻿using Individuele_Opdracht___Facturatiesysteem.State.Navigators;
using System.Windows.Input;

namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        public INavigator Navigator { get; set; } = new Navigator();

        //private ViewModelBase _selectedViewModel = new StartViewModel();

        //public ViewModelBase SelectedViewModel
        //{
        //    get { return _selectedViewModel; }
        //    set
        //    {
        //        _selectedViewModel = value;
        //        OnPropertyChanged(nameof(SelectedViewModel));
        //    }
        //}

        //public ICommand UpdateViewCommand { get; set; }

        //public MainViewModel()
        //{
        //    UpdateViewCommand = new UpdateViewCommand(this);
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class PersoneelslidDatabeheerViewModel : ViewModelBase
    {
        private ObservableCollection<Personeelslid> _personeelsleden;

        public ObservableCollection<Personeelslid> Personeelsleden
        {
            get { return _personeelsleden; }
            set
            {
                _personeelsleden = value;
                OnPropertyChanged(nameof(Personeelsleden));
            }
        }


        public PersoneelslidDatabeheerViewModel()
        {
            Personeelsleden = new ObservableCollection<Personeelslid>(DataManager.GetPersoneelsleden());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class PersoneelslidOverzichtViewModel:ViewModelBase
    {
        public ObservableCollection<Personeelslid> Personeelsleden { get; set; }

        public PersoneelslidOverzichtViewModel()
        {
            Personeelsleden = new ObservableCollection<Personeelslid>(DataManager.GetPersoneelsleden());
        }
    }
}

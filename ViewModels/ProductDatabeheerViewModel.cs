﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class ProductDatabeheerViewModel : ViewModelBase
    {
        private ObservableCollection<Product> _producten;

        public ObservableCollection<Product> Producten
        {
            get { return _producten; }
            set
            {
                _producten = value;
                OnPropertyChanged(nameof(Producten));
            }
        }


        public ProductDatabeheerViewModel()
        {
            Producten = new ObservableCollection<Product>(DataManager.GetProducten());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class ProductOverzichtViewModel : ViewModelBase
    {
        public ObservableCollection<Product> Producten { get; set; }

        public ProductOverzichtViewModel()
        {
            Producten = new ObservableCollection<Product>(DataManager.GetProducten());
        }
    }
}

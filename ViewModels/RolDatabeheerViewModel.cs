﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class RolDatabeheerViewModel : ViewModelBase
    {
        private ObservableCollection<Rol> _rollen;

        public ObservableCollection<Rol> Rollen
        {
            get { return _rollen; }
            set
            {
                _rollen = value;
                OnPropertyChanged(nameof(Rollen));
            }
        }


        public RolDatabeheerViewModel()
        {
            Rollen = new ObservableCollection<Rol>(DataManager.GetRollen());
        }
    }
}

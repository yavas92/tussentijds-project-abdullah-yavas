﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class RolOverzichtViewModel : ViewModelBase
    {
        public ObservableCollection<Rol> Rollen { get; set; }

        public RolOverzichtViewModel()
        {
            Rollen = new ObservableCollection<Rol>(DataManager.GetRollen());
        }
    }
}

﻿namespace Individuele_Opdracht___Facturatiesysteem.ViewModels
{
    public class StartViewModel : ViewModelBase
    {
        private readonly Personeelslid model;

        public StartViewModel()
        {
            this.model = new Personeelslid();
        }

        public string Username { get => App.LoggedInUser.Username; }
        public bool IsAdmin { get => App.LoggedInUser.Role == Roles.Administrator; }
        public bool IsVerkoper { get => App.LoggedInUser.Role == Roles.Verkoper; }
        public bool IsMagazijnier { get => App.LoggedInUser.Role == Roles.Magazijnier; }

    }
}

﻿using Individuele_Opdracht___Facturatiesysteem.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Individuele_Opdracht___Facturatiesysteem.Views
{
    /// <summary>
    /// Interaction logic for KlantView.xaml
    /// </summary>
    public partial class KlantOverzichtView : UserControl
    {
        public KlantOverzichtView()
        {
            InitializeComponent();
            DataContext = new KlantOverzichtViewModel();
        }
    }
}

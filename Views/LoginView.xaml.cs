﻿using System;
using System.Windows;
using System.Windows.Controls;
using Individuele_Opdracht___Facturatiesysteem.ViewModels;

namespace Individuele_Opdracht___Facturatiesysteem.Views
{
    /// <summary>  
    /// Interaction logic for MainWindow.xaml  
    /// </summary>   
    public partial class LoginView : Window
    {
        public LoginView() // Viewmodel parameter verwijderd
        {
            InitializeComponent();
            DataContext = new LoginViewModel(); // Toegevoegd 21/10
        }


        private void button1_Click(object sender, RoutedEventArgs e)
        {
            string username = txtLogin.Text;
            string password = txtWachtwoord.Password;

            var login = DataManager.GetLogin(username, password);


            if (username.Length == 0)
            {
                MessageBox.Show("Geef een geldige login in.", "Ongeldige input", MessageBoxButton.OK, MessageBoxImage.Warning);
                txtLogin.Focus();
            }
            else
            {
                if (login != null)
                {
                    var roles = Enum.GetValues(typeof(Roles));
                    App.LoggedInUser.Username = login.Username;
                    switch (login.RolID)
                    {
                        case 1:
                            App.LoggedInUser.Role = Roles.Administrator;
                            break;
                        case 2:
                            App.LoggedInUser.Role = Roles.Verkoper;
                            break;
                        case 3:
                            App.LoggedInUser.Role = Roles.Magazijnier;
                            break;
                        default:
                            break;
                    }
                    Close();
                }
                else
                    MessageBox.Show("Sorry! Please enter existing username/password.", "Ongeldige input", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
﻿using Individuele_Opdracht___Facturatiesysteem.ViewModels;
using System.Windows;

namespace Individuele_Opdracht___Facturatiesysteem.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainViewModel();
        }
    }
}

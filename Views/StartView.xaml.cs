﻿using Individuele_Opdracht___Facturatiesysteem.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace Individuele_Opdracht___Facturatiesysteem.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class StartView : UserControl
    {
        public StartView()
        {
            InitializeComponent();
            DataContext = new StartViewModel();
        }
    }
}
